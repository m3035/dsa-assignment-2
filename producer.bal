import ballerinax/kafka;


kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    string message = "Hello World, Ballerina";
    check kafkaProducer->send({
                                topic: "dsa.assignment2",
                                value: message.toBytes() });

    check kafkaProducer->'flush();
}